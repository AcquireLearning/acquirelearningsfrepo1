trigger leadTrigger on Lead (before update) {
    if(Trigger.isBefore && Trigger.isUpdate) {
        leadUtil.removeAttachments(Trigger.New, Trigger.oldMap);
    }
}
/*
trigger leadTrigger on Lead (after update) {
    if(Trigger.isAfter && Trigger.isUpdate) {
        leadUtil.removeAttachments(Trigger.New, Trigger.oldMap);
    }
}
*/