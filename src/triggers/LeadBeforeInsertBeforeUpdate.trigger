trigger LeadBeforeInsertBeforeUpdate on Lead (before insert, before update) {

    /************************
    * Note:    Coppied entire code into LeadUtlityTrigger & LeadTriggerHandler class
    *          as per SFDC best practice.  One object should have only one active trigger.
    *
    * Trigger:      LeadBeforeInsertBeforeUpdate
    * Author:       Michael Cooksley (Cloud Sherpas)
    * Created:      12-11-2013
    * Description:  When a lead is updated and the status is set to Qualified for Contact then
    *               compare various fields on the lead object against a Meta data object to
    *               determine the Lead Rating. Update the Lead with this Rating.
    * Version:      1.0
    * Change Log:   1.1 - 19-11-13: MC Added Queue logic and modifed trigger to handle before insert.
    *
    * 
    ************************/


}