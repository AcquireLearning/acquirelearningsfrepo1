trigger welcomeEventOnOppOrAcc on Event (after insert) {

    Set<String> whatOpIDs = new Set<String>();
    Set<String> whatAcIDs = new Set<String>();

    //get record IDs related to Welcome Call events
    for (Event ev : Trigger.new) {
        if (ev.Subject =='Welcome Call Booking' && ev.whatID.getSObjectType().getDescribe().getName() == 'Opportunity') {
            whatOpIDs.add(ev.whatID);
            System.Debug('Welcome call message found related to ' + ev.whatID.getSObjectType().getDescribe().getName());
        } else if (ev.Subject =='Welcome Call Booked for Champion by Eddi' && ev.whatID.getSObjectType().getDescribe().getName() == 'Account') {
            whatAcIDs.add(ev.whatID);
            System.Debug('Welcome call message found related to ' + ev.whatID.getSObjectType().getDescribe().getName());
        }
    }
 
    if (whatOpIds.Size() >= 1) { 
        //get opportunities related to events 
        List<Opportunity> opps = [SELECT Id, Welcome_Call_scheduled__c FROM Opportunity WHERE Id =: whatOpIDs];
        
        //update related opportunity Welcome Call scheduled field to true
        for (Opportunity o : opps) {
            if (o.Welcome_Call_scheduled__c == FALSE) {
                o.Welcome_Call_scheduled__c = TRUE;
                System.Debug('Welcome call scheduled on Opp set to true');
            }
        }
    
        update opps;
    }
    if (whatAcIds.Size() >= 1) { 
        //get accounts related to events 
        List<Account> accs = [SELECT Id, Welcome_Call_booked_on_account__c FROM Account WHERE Id =: whatAcIDs];
        
        //update related account Welcome call booked on account field to true
        for (Account a : accs) {
            if (a.Welcome_Call_booked_on_account__c == FALSE) {
                a.Welcome_Call_booked_on_account__c = TRUE;
                System.Debug('Welcome call booked on account set to true');
            }
        }
    
        update accs;
    }

}