trigger CourseDatesTrigger on Opportunity (before insert, before update) {
    
    // if there's an enrolment date and it's been changed, and there's no start date
	if (!CourseDatesMap.HasAlreadyCheckedDates())
    {
        if (Trigger.isUpdate)
        {
            // if start date is present and has changed
            if (trigger.old[0].Course_Start_Date__c != trigger.new[0].Course_Start_Date__c && trigger.new[0].Course_Start_Date__c != NULL){
                // provide course dates based on Start Date
                System.debug('Updated opp to be based on Start date');
                CourseDatesMap.mapCourseDates(trigger.new);
            } else {
                 System.debug('Start date not changed or null');
            }

        } else if (Trigger.isInsert)
        {
            // if there's a start date
            if (trigger.new[0].Course_Start_Date__c != NULL){
                // provide course dates based on Start Date
                CourseDatesMap.mapCourseDates(trigger.new);
                System.debug('New created opp to be based on Start date');
            } else {
                System.debug('Start date is null');
            }
        }
        CourseDatesMap.setAlreadyCheckedDates();
    }

}