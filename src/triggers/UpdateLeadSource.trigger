trigger UpdateLeadSource on Lead (before insert, before update) {


if(Trigger.isUpdate)
 {
 if(trigger.old[0].LeadSource == null &&
 trigger.old[0].Lead_Sources_for_Career_Advisor__c != trigger.new[0].Lead_Sources_for_Career_Advisor__c
 )
     {
      trigger.new[0].LeadSource = trigger.new[0].Lead_Sources_for_Career_Advisor__c ; 
     }
else if(trigger.old[0].LeadSource == null &&
  trigger.old[0].Lead_Source_for_Employer__c != trigger.new[0].Lead_Source_for_Employer__c
 )
     {
      trigger.new[0].LeadSource = trigger.new[0].Lead_Source_for_Employer__c ; 
     }
  }
else if (Trigger.isInsert)
 {
     if(trigger.new[0].Lead_Sources_for_Career_Advisor__c != null)
     {
      trigger.new[0].LeadSource = trigger.new[0].Lead_Sources_for_Career_Advisor__c ;  
     }
     else if(trigger.new[0].Lead_Source_for_Employer__c != null)
     {
      trigger.new[0].LeadSource = trigger.new[0].Lead_Source_for_Employer__c ;    
     }
  }



}