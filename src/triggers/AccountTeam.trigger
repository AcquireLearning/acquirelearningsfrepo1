trigger AccountTeam on Account (after update) {
    
    Integer newcnt = 0;
    Integer newcnt0 = 0;
    List<Id> accOwnerIds = new List<Id>();
    AccountTeamMember[] newmembers = new AccountTeamMember[]{};
    //list of new team members to add
    AccountShare[] newShare = new AccountShare[]{};
    // Get users owning updated records
    for(Account acc: trigger.newMap.values())
    {
        accOwnerIds.add(acc.ownerid);
    }
    
    Map<id,user> mpuser=new Map<id,user>([select id, name, profile.name from user where id in :accOwnerIds]);
    for(Account newAcc: trigger.newMap.Values()){
                
        String newOwnerProfile = (mpuser.get(trigger.newMap.get(newAcc.id).ownerid).profile.name).trim();
        if (trigger.old[0].OwnerId != newAcc.OwnerId
            && (newOwnerProfile == 'Acquire Career Champion' || newOwnerProfile == 'Eddi Edvisor')
            && newAcc.Opportunity_Stage__c != 'Course Deffered Requested' 
            && newAcc.Opportunity_Stage__c != 'Course Deffered Confirmed' 
            && newAcc.Opportunity_Stage__c != 'Cancellation Requested' 
            && newAcc.Opportunity_Stage__c != 'Cancellation Confirmed') {
                
                System.Debug('New team member to be added');
                AccountTeamMember Teammemberad=new AccountTeamMember();
                Teammemberad.AccountId = newAcc.id;
                Teammemberad.UserId = newAcc.OwnerId;
                if (newOwnerProfile == 'Acquire Career Champion') {
                    Teammemberad.TeamMemberRole = 'Career Champion';
                    System.Debug('Career Champion team member added: ' + newAcc.OwnerId);
                }
                else if (newOwnerProfile == 'Eddi Edvisor') {
                    Teammemberad.TeamMemberRole = 'Eddi Trainer / Assessor';
                    System.Debug('Eddi Trainer / Assessor team member added: ' + newAcc.OwnerId);
                }
                newmembers.add(Teammemberad);
            }
        } 
    Database.SaveResult[] lsr = Database.insert(newmembers,false);
    //insert any valid members then add their share entry if they were successfully added Integer newcnt=0;
    for(Database.SaveResult sr:lsr) 
    {
        if(!sr.isSuccess()) {
            Database.Error emsg =sr.getErrors()[0];
            
            system.debug('\n\nERROR ADDING TEAM MEMBER:'+emsg);
        } else {
            //Create sharing for new account member
            newShare.add(new AccountShare(UserOrGroupId=newmembers[newcnt].UserId, AccountId=newmembers[newcnt].Accountid, AccountAccessLevel='All',OpportunityAccessLevel='Read'));
        }
        newcnt++;
    }
    
    //Give outgoing Owner edit access to Account
    try {
        AccountShare oldOwnerShare = [select AccountId, AccountAccessLevel, UserOrGroupId from AccountShare where AccountId =: trigger.old[0].Id and UserOrGroupId =: trigger.old[0].OwnerId limit 1];
        oldOwnerShare.AccountAccessLevel = 'Edit';
        system.debug('Old owner ' + trigger.old[0].OwnerId + ' accountshare updated to write'); 
        update oldOwnerShare;
    } catch(exception ex){
        system.debug('ex ############:' + ex);
    } 
    Database.SaveResult[] lsr0 =Database.insert(newShare,false);
    //insert the new shares Integer newcnt0=0;
    for(Database.SaveResult sr0:lsr0) {
        if(!sr0.isSuccess()) {
            Database.Error emsg0 = sr0.getErrors()[0];
            system.debug('\n\nERROR ADDING SHARING:'+newShare[newcnt0]+'::'+emsg0); 
        } 
        newcnt0++; 
    }


}