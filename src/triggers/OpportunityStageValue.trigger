trigger OpportunityStageValue on Opportunity (before update, after insert,after update) {

	if(trigger.isAfter){
		AccountTrgUtil.updateAcc(trigger.newMap);
	}else if(trigger.isBefore) {
		AccountTrgUtil.updateOppOwner(trigger.oldMap, trigger.newMap);
	}
}