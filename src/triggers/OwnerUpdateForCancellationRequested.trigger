trigger OwnerUpdateForCancellationRequested on Account (before update,after update) {
   if(trigger.IsBefore){
       AccountTrgUtil.updateAccountOwner(trigger.oldMap, trigger.newMap );
       
         if(trigger.isUpdate){
           AccountTrgUtil.createCampaignMember(trigger.newMap);
           AccountTrgUtil.createVentureCampaignMember(trigger.New,trigger.OldMap);
       }
   }    

   if(trigger.isAfter){
       AccountTrgUtil.updateOppStage(trigger.newMap);
   }

}