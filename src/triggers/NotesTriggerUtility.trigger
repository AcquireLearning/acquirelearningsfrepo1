trigger NotesTriggerUtility on Note (after insert,before delete) {

     //this handler method is used to count number of attachments in Lead object
    NotesAndAttachmentTriggerHandler.CountNotesOnLead(trigger.new, trigger.old, trigger.isInsert, trigger.isDelete);

}