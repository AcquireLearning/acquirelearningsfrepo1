@istest
public class countCVTest 
{
    static testMethod void countCVTest() 
    {
        
        RecordType accrt = [select id,Name from RecordType where SobjectType='Account' and Name='Business Account' Limit 1];
        //BL_AccountFileUpload controller=new BL_AccountFileUpload(new ApexPages.StandardController(acc));

        Account acc = new Account();
        acc.RecordTypeId = accrt.id;
        acc.Name='test';
        insert acc;
    
        Attachment at = new Attachment();
        at.Name = 'CV_test';
        at.parentid=acc.Id;
        at.Body=blob.valueof('b');
        insert at;
        
        
        //controller.fileName='CV_test';
        //controller.fileBody=Blob.valueOf('CV Test Attachment Body');
        //controller.uploadFile();
        
        //List<Attachment> attachments=[select id, name from Attachment where Name=:acc.id];
        //System.assertEquals(1, attachments.size());
        
        Account createdAcc = [Select Id,Name,CV_attached__c From Account Where Id = :acc.Id];
        
        System.debug('Account ' + createdAcc.Id + ' ' + createdAcc.Name + ' - CV attached: ' + createdAcc.CV_attached__c);
    }

}