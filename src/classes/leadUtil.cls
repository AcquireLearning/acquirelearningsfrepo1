public class leadUtil {
    
    public static void removeAttachments(list<Lead> newList, map<Id,Lead> oldMap) {
        set<Id> setIDs = new set<Id>();
        for(Lead l : newList) {
            if(l.IsConverted == true && oldMap.get(l.Id).isConverted == false) {
                //We have a converted lead
                if(l.ConvertedContactId != null) setIds.add(l.ConvertedContactId);
                if(l.ConvertedAccountId != null) setIds.add(l.ConvertedAccountId);
                setIds.add(l.Id);
            }
        }
        
        list<Attachment> listAtt = new list<Attachment>();
        listAtt = [select id,Name from Attachment where ParentId in: setIds];
        list<Attachment> listDeleteAtt = new list<Attachment>();
        if(!listAtt.isEmpty()) {
            for(Attachment a : listAtt) {
            	if(a.Name.startsWith('Lead_')) { //We have a lead attachment to delete
            		listDeleteAtt.add(a);
            	}
            }
        }
        
        if(!listDeleteAtt.isEmpty()) {
            try {
                delete listDeleteAtt;
            } catch (Exception ex) {
                system.debug('Error deleting attachment: ' + ex.getMessage());
            }
        }
    }
}