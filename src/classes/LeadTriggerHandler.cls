public with sharing class LeadTriggerHandler{

/************************
    * Trigger:      LeadUtilityTrigger
    * Author:       Offshore Development
    * Created:    12-11-2013

    * Version:    1.0
    * Change Log:  1.1 - 19-11-13: MC Added Queue logic and modifed trigger to handle before insert.
    *
    * 
    ************************/

  // Variables
  private static Map<Id, Meta_Rating__c> ratingMap;
  private static Map<Id, Meta_Lead_Source__c> leadSourceMap;
  private static Map<Id, Group> queueIdMap;
  private static List<Employer_Meta_Rating__c> emplRatingList;
  private static String Rating = '';
  private static String Queue = '';
    
    /**
    * Description:  When a lead is updated and the status is set to Qualified for Contact then
    *        compare various fields on the lead object against a Meta data object to
    *        determine the Lead Rating. Update the Lead with this Rating.
    */
    public static void updateLeadRating(Map<id, Lead> oldLeadMap, List<Lead> newLeadList, Boolean isInsert, Boolean isUpdate){
        // Load all rating meta data into memory
        ratingMap = new Map<Id, Meta_Rating__c>(
            [SELECT Id, Age_Lower__c, Age_Upper__c,Education__c, Gender__c,Rating__c
            FROM Meta_Rating__c]);
            
      // Load all lead source meta data into memory
      leadSourceMap = new Map<Id, Meta_Lead_Source__c>(
            [SELECT Id, Queue_Name__c, Lead_Source__c, Category__c
            FROM Meta_Lead_Source__c
            WHERE Type__c = 'Queue']);
    
      // Load all queue data into memory
      queueIdMap = new Map<Id, Group>(
            [SELECT Id, Name
            FROM Group
            WHERE Type = 'Queue']);

        // Work through the leads that have had their status updated to "Qualified for Contact".

        for(Lead l : newLeadList) {     
         //  if((isUpdate && l.Status != oldLeadMap.get(l.Id).Status && l.Status == 'Qualified for Contact') ||
         //  (isInsert && l.Status == 'Qualified for Contact')) {
                if(l.Status == 'Qualified for Contact') {
                  
                  
                // If the status has tranisitioned from New to Qualified for Contact then assign to Queue based on lookup table.
           //     if((isUpdate && oldLeadMap.get(l.Id).Status == 'New') || isInsert )
            //    {
                  
                   try{
                        for(Id leadSourceId : leadSourceMap.keySet())
                        {
                            Meta_Lead_Source__c leadSourceObject = leadSourceMap.get(leadSourceId);
                            if (leadSourceObject.Lead_Source__c == l.LeadSource){
                                if (leadSourceObject.Category__c.contains(l.Category__c)){
                                    if((isUpdate && l.Status != oldLeadMap.get(l.Id).Status && l.Status == 'Qualified for Contact') ||
                                   (isInsert && l.Status == 'Qualified for Contact')) {
                                        if((isUpdate && oldLeadMap.get(l.Id).Status == 'New') || isInsert ) {
                                        
                                            Queue = leadSourceObject.Queue_Name__c;
                                        }
                                        
                                }
                                        
                                     
                                }
                            }
                            
                        }
                    }catch(exception ex){
                        // Do nothing.
                    }
               // }
                
                // Loop through each level of the Map to find a matching record.
                // At most levels a comparison is done with contains as the picklist has a unqiue set
                // for the Education comparison a string split is required as the picklist contains Diploma and Advanced Diploma.
                try{
                    for(Id ratingId : ratingMap.keySet())
                    {
                        Meta_Rating__c ratingObject = ratingMap.get(ratingId);
                       // if (ratingObject.Lead_Source__c.contains(l.LeadSource)){
                          //  if (ratingObject.Category__c.contains(l.Category__c)){
                                if (ratingObject.Gender__c.contains(l.Gender__c)){
                                    if(ratingObject.Age_Lower__c <= l.Age__c && ratingObject.Age_Upper__c >= l.Age__c){
                                        List<String> eduList = ratingObject.Education__c.split(';');
                                        for (String edu : eduList){
                                            if (edu == l.Highest_Education__c){
                                                Rating = ratingObject.Rating__c;
                                            }
                                        }
                                    }
                                }
                            //}
                       // }  
                    }
                }
                catch(exception ex){
                    l.Rating__c = 'UNK';
                }

                // If we have a rating then update Lead Rating field.
                if(Rating != ''){
                    l.Rating__c = Rating;
                }else{
                    l.Rating__c = 'UNK';
                }
                
                // If we have a queue name then lookup the Map and get the queue Id before 
                // assigning this to be the owner of the lead.
                if(Queue != ''){
                    for(Id queueId : queueIdMap.keySet()){
                        Group queueObject = queueIdMap.get(queueId);
                        if (queueObject.Name == Queue){
                            l.OwnerId = queueId;
                        }
                    }   
                }
           // }
            }
        }        
    }
    
   
    public static void NoConversionOnNewAndUnableToCallStatus(Map<Id, Lead> oldLeadMap, Map<Id, Lead> newLeadMap){
        List<Lead> newLeadList = newLeadMap.Values();
        
        for(Lead leadrec : newLeadList) {
            if(oldLeadMap.get(leadrec.Id).IsConverted != newLeadMap.get(leadrec.Id).IsConverted)
            {
                if(oldLeadMap.get(leadrec.Id).Status == 'New'|| oldLeadMap.get(leadrec.Id).Status == 'Unable to Call')
                    leadrec.addError('Conversion allowed only for Qualified for contact leads');
            }
        
        }
    }
    
    /**
    * Description:  This method is used to update lead rating based on classification and
    * sub-classification values in employer meta rating object. 
    */    
    
    public static void UpdateEmpolyerLeadRating(List<Lead> newLeadList){
        RecordType recType = [Select id, name, DeveloperName From RecordType WHERE SobjectType = 'Lead' AND DeveloperName = 'Employer' LIMIT 1];
        
        // get all Employer meta rating records
        emplRatingList = 
            [Select Id, Name, Classification__c, Sub_Classification__c, Classification_Rating__c, Job_Location__c, Area_Specific__c, Job_Location_State__c, Location_Rating__c
            From Employer_Meta_Rating__c];
            
        for(Lead lead: newLeadList){
            if(lead.RecordTypeId == recType.Id){
                for(Employer_Meta_Rating__c emplMetaRating: emplRatingList){
                    if(lead.Classification__c == emplMetaRating.Classification__c && 
                        lead.Sub_Classification__c == emplMetaRating.Sub_Classification__c){
                            lead.Classification_Rating__c = emplMetaRating.Classification_Rating__c;
                    } 
                    
                    if(lead.Job_Location__c == emplMetaRating.Job_Location__c &&
                                lead.Job_Location_State__c == emplMetaRating.Job_Location_State__c &&
                                lead.Area_Specific__c == emplMetaRating.Area_Specific__c){
                            lead.Location_Rating__c = emplMetaRating.Location_Rating__c;
                    }
                }
            }
        }
    }
}