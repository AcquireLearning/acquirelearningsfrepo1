@isTest
private class attachmentUtil_Test {

    static testMethod void myTest() {

        Lead l1 = new Lead(LastName = 'Contact1',Company = 'Test1',Status = 'Open');
        insert l1;
        
        Account a1 = new Account(Name = 'testAcc');
        insert a1;              
                        
        String attachmentData = 'StartDate,EndDate,PayrollGroupName,EmpGroupName,' +
            'EmpName,ApprovedBy,Payrate,RegHours,Ot1Hours,Ot2Hours,' +
            'Other1Hours,Other2Hours,Other3Hours,Other4Hours,Other5Hours,' +
            'RegCategoryName,Ot1CategoryName,Ot2CategoryName,' +
            'Other1CategoryName,Other2CategoryName,Other3CategoryName,' +
            'Other4CategoryName,Other5CategoryName,RegCategoryCode,' +
            'Ot1CategoryCode,Ot2CategoryCode,Other1CategoryCode,' +
            'Other2CategoryCode,Other3CategoryCode,Other4CategoryCode,' +
            'Other5CategoryCode\n';         
        Blob body1 = Blob.valueOf(attachmentData);
        
        Attachment attachment = new Attachment(
            body=body1,
            Name='signatureTest.gif',
            ContentType='image/gif',
            ParentId = l1.Id);
            
        Attachment attachment2 = new Attachment(
            body=body1,
            Name='signatureTest.gif',
            ContentType='image/gif',
            ParentId = a1.Id);    
                
        Test.startTest();
            //Test whether our new attachment get the lead prefix
            insert attachment;
            String res = [select Name from Attachment where Id =: attachment.id].Name;
            system.assertEquals(res.startsWith('Lead_'),true);
            //Test that an attachment to anything BUT a lead has no prefix
            insert attachment2;
            res = [select Name from Attachment where Id =: attachment2.id].Name;
            system.assertEquals(res.startsWith('Lead_'),false);
                        
                
    }   
}