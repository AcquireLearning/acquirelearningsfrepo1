global class CreateNewLeadfromEmail implements Messaging.InboundEmailHandler {
    /************************
    * Class:        CreateNewLeadfromEmail
    * Author:       Michael Cooksley (Cloud Sherpas)
    * Created:      12-11-2013
    * Description:  Takes inbound emails, gets the information from the email and creates a lead.
    *               Any email attachments are created on the lead. If the reading of the email or any
    *               part of this code fails then a generic lead will be created with as much information
    *               as possible from the email. The result.success is always true so we dont return an error message.
    *               It is assumed the email.toAddresses only ever has one address in it as emails are fowarded from each address.
    * Version:      1.0
    * Change Log:   1.1 - 18-11-13: MC - Updated to set Lead Source and Fixed issue with reading inbound ToAddress.
    *               1.2 - 26-11-13: MC - Updated to check if plainTextBody is empty.
    *               1.3 - 27-11-13: MC - Added check for special chars ' and space removal in HTML inbound applicant name.
    *               1.4 - 02-12-13: MC - Added code to log inbound email address to fields on lead.
    ************************/
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.inboundEmail email, Messaging.InboundEnvelope env){
    
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        
        // Create some local variables  
        String myEmailBody= '';
        String applicantName = ''; 
        String applicantFirstName = '';
        String applicantLastName = '';
        String applicantEmail = '';
        String applicantDate;
        String applicantPhone = '';
        String applicantCategory = '';
        String inboundEmailAddress = '';
        String emailTo = '';
        String newLeadId = '';
        String domain = '';
        String exMessage = '';
        
        
        // Result will always be set to true as we dont want to return an email if there is any error.
        result.success = true;
        system.debug('******');
        try{
            
            // Get the information from the email body used for creating the lead record.
            // check if the plainTextBody is null.
            if(email.plainTextBody != null){
                myEmailBody = email.plainTextBody;
                applicantName = myEmailBody.substringBetween('Applicant Name: ','\n');
                applicantEmail = myEmailBody.substringBetween('Email: ','\n');
                applicantDate = myEmailBody.substringBetween('Application Date: ','\n');
                applicantPhone = myEmailBody.substringBetween('Phone: ','\n');
            }else{
                myEmailBody = email.htmlBody;
                applicantName = myEmailBody.substringBetween('Applicant Name: ','<br />');
                applicantEmail = myEmailBody.substringBetween('Email: <a href=\'mailto:','\'>');
                applicantDate = myEmailBody.substringBetween('Application&nbsp;Date: ','<br />');
                applicantPhone = myEmailBody.substringBetween('Phone: ','<br />');
            }
            System.Debug('==== executing email body log ==== \n' + myEmailBody);
            System.Debug('\n ==== email body log end ==== ');       
            //Date appDate = Date.today();
            //Date appDate = Date.parse(applicantDate);
                
            // Get the inbound email address. Split and use to determine what Category the lead is assigned to.
            // It is assumed the address would be in the format: applicants@recruiteasy.com.au, beautyapplicants@recruiteasy.com.au
            // itapplicants@recruiteasy.com.au, applicants@quicktemps.com.au, itapplicants@quicktemps.com.au
            // inboundEmailAddress = email.fromAddress;
            // We can assume there is only one address in the toAddresses as the email is being forwarded.
            inboundEmailAddress = email.toAddresses[0];
            List<String> emailParts = inboundEmailAddress.split('applicants@');
            emailTo = emailParts[0];
            
            // if there is no emailTo then we know this is a general category otherwise assign the emailTo to the category.
            if(emailTo == '' || emailTo == null){
                applicantCategory = 'General';
            }
            else {
                applicantCategory = emailTo;
            }

            List<String> domainParts = inboundEmailAddress.split('@');
            domain = domainParts[1];
            system.debug('domain ##########:' + domain);
            List<Meta_Lead_Source__c> leadSourceList = [SELECT Lead_Source__c FROM Meta_Lead_Source__c WHERE Type__c = 'Lead Source' AND Domain__c = :domain LIMIT 1];
            
             system.debug('*****' + leadSourceList );
             
            String leadSource = leadSourceList[0].Lead_Source__c;
             system.debug('*****' + leadSource );   
             
            // Split the Applicant name to get firstname and lastname
            List<String> parts = applicantName.split(' ');
            applicantFirstName = parts[0];
            applicantLastName = parts[1];

            // if we have html email body then remove any special chars from first and last name.
            if(email.htmlBody != null){
                // remove any special html chars from FirstName
                if(applicantFirstName.contains('&#32')){
                    applicantFirstName = applicantFirstName.replace('&#32', ' ');
                }else if (applicantFirstName.contains('&#39')){
                    applicantFirstName = applicantFirstName.replace('&#39', '\'');
                }

                // remove any special html chars from LastName
                if(applicantLastName.contains('&#32')){
                    applicantLastName = applicantLastName.replace('&#32', ' ');
                }else if (applicantLastName.contains('&#39')){
                    applicantLastName = applicantLastName.replace('&#39', '\'');
                }
            }
            
            // Create a new Lead
            Lead newlead = new Lead();
            newlead.Email = applicantEmail;
            newlead.LastName = applicantLastName;
            newlead.FirstName = applicantFirstName;
            newlead.LeadSource = leadSource;
            newlead.Phone = applicantPhone;
            newlead.Category__c = applicantCategory;
           // newlead.Application_Date__c = appDate;
            newlead.Email_Body__c = myEmailBody;
            newlead.Email_Forward_Address__c = inboundEmailAddress;

            // Use Assignment Rules when creating the lead.
            Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.assignmentRuleHeader.useDefaultRule = true;
            newLead.setOptions(dmo);
    
            // Insert a new lead
            insert newLead; 
            newLeadId = newLead.Id;
        }
        catch(exception aex)
        {
            system.debug('***'+ aex);
            // Create lead above failed. Create and empty lead with as much
            // detail as possible from the inbound email.
            Lead newemptylead = new Lead();
            newemptylead.Email = email.fromAddress;
            newemptylead.LastName = 'Admin - For Review';
            newemptylead.FirstName = 'System';
            newemptylead.Email_Body__c = myEmailBody;
            newemptylead.Email_Forward_Address__c = inboundEmailAddress;

            // Insert a new lead
            insert newemptylead;
            newLeadId = newemptylead.Id;    
        } 
        
        try{    
            // Insert any attachments on the lead.          
            if((email.binaryAttachments != null && email.binaryAttachments.size() > 0) || (email.textAttachments != null && email.textAttachments.size() > 0)){
                List<attachment> aList = new List<attachment>();    
                
                if(email.binaryAttachments != null && email.binaryAttachments.size() > 0){
                    for (integer i = 0 ; i < email.binaryAttachments.size() ; i++){
                        aList.add(new Attachment(ParentId = newLeadId, Name = email.binaryAttachments[i].filename, Body = email.binaryAttachments[i].body));
                    }
                }
                if(email.textAttachments != null && email.textAttachments.size() > 0){
                    for (integer j = 0 ; j < email.textAttachments.size() ; j++){
                        aList.add(new Attachment(ParentId = newLeadId, Name = email.textAttachments[j].filename, Body = Blob.valueOf(email.textAttachments[j].body)));          
                    }
                }
            
            insert aList;
            }   
        } 
        catch (exception aex){
            // if error on attachment then do nothing.
        }
        
        // Return the result for the Apex Email Service 
        return result;
    }
}