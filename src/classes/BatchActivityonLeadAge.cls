global class BatchActivityonLeadAge implements Database.Batchable<sObject>{
  /************************
    * Class:        BatchActivityonLeadAge
    * Author:       Offshore Development
    * Created:      20/03/2014
    * Description:  Lead owner is set to a Queue when Last Activity was "No Answer" ,
                     Lead status is not equal to Closed and when the number set on custom setting is 
                      greater than (Today - LastModifiedDate)
    * Version:      1.0
    
    ************************/
    
Integer lage;
global BatchActivityonLeadAge(){
    }
    // Getting value from custom setting Lead_Age__c
     global Database.QueryLocator start(Database.BatchableContext BC){
         Lead__c leadage=[select Lead_Age__c from Lead__c limit 1]; 
           lage =(Integer)leadage.Lead_Age__c;
         
        
        return Database.getQueryLocator([Select Status,LeadSource,Lead_Age__c,RecordType.Name,lastmodifieddate,(SELECT whoid, subject, status FROM Tasks order by createddate DESC limit 1),OwnerId from Lead where status!='Closed' AND ( LeadSource = 'Acquire Inbound Phone' OR LeadSource = 'Acquire Inbound Chat' OR LeadSource = 'Acquire Inbound Web' OR LeadSource = 'Carlton FC' OR LeadSource = 'Seek' 
        OR LeadSource = 'Career One (Employer)' OR LeadSource = 'My Career' OR LeadSource = 'Indeed' OR LeadSource = 'Spot Jobs (Employer)'
        OR LeadSource = 'Gumtree' OR LeadSource = 'Free Job Sits' OR LeadSource = 'Adzuna' OR LeadSource = 'Employer Referral')]);
        }
        
           global void execute(Database.BatchableContext info, List<sObject> scope){
            List<Lead> LDList = new List<Lead>();          
            
            List<QueueSobject> lstQueues = [SELECT Id,queue.Name,SobjectType,QueueId  FROM QueueSobject WHERE SobjectType = 'Lead'and queue.Name = 'AU Inbound Unable to Contact'];
            List<QueueSobject> lstQueues1 = [SELECT Id,queue.Name,SobjectType,QueueId  FROM QueueSobject WHERE SobjectType = 'Lead'and queue.Name = 'Employer leads unable to contact'];
            
            // Group queueID = [Select Id, Name, Email from Group where Type = 'Queue' And Name = 'Inbound_Lead_Age_7_days' Limit 1].ID ;  
            Integer diff; 
            Lead__c leadage=[select Lead_Age__c from Lead__c limit 1];
            for(sobject L: scope){
            Lead ll=(Lead)L;  
           // diff = System.today()-(Date.Valueof (ll.lastmodifieddate));
            Date lastModifedwitLeadDage = Date.valueOF(ll.lastmodifieddate).addDays( Integer.valueOF(leadage.Lead_Age__c)) ;
            system.debug('ll.Tasks.size()*****'+ ll.Tasks.size());
            //System.debug('ll.Tasks[0].subject*****'+ ll.Tasks[0].subject);
                //if(diff > leadage){
            Date tday = System.today() ;      
                 // if(String.ValueOf(ll.LeadSource)!= null && String.ValueOf(ll.LeadSource).Contains('Inbound'))        
                    if((ll.Tasks!=Null && ll.Tasks.size()>0) && ll.Tasks[0].subject=='No Answer' && tday > lastModifedwitLeadDage ) {
                        if(ll.RecordType.Name == 'Employer'){
                            ll.OwnerId=lstQueues1[0].QueueId;
                            LDList.add(ll); 
                        }
                        else{
                            ll.OwnerId=lstQueues[0].QueueId;
                            LDList.add(ll);
                        }
                                   
                 
                }
            }
            update LDList;
    
    }
    
     global void finish(Database.BatchableContext BC){
   } 
        
    
    }