/* abc*/  
@istest
public class AccountProfile_test    
{
    public static testMethod void accountProfileTest() 
    {
        Profile profile = [SELECT Id FROM Profile WHERE Name='Acquire Career Champion']; 
        
        User user = new User(Alias = 'standt', Email='test.user@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = profile.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='test.acquireLearning@testorg.com');
        insert user;
        
        RecordType personacc = [select id,Name from RecordType where SobjectType='Account' and Name='Person Account' Limit 1];
        
        Account Acc = new Account();
        Acc.RecordTypeId = personacc.id;
        Acc.LastName='Test';
        insert Acc;
        
        Account createdAcc = [Select Id,LastName,OwnerId From Account Where Id = :Acc.Id];
        createdAcc.OwnerId = user.Id;
        update createdAcc;
        
        Account createdAcc2 = [Select Id,LastName,OwnerId,Owner_Profile__c From Account Where Id = :CreatedAcc.Id];
                        
        System.debug('Account Owner' + createdAcc2.OwnerId + ' Profile recorded on account record as ' + createdAcc2.Owner_Profile__c);
        system.assertEquals (createdAcc2.Owner_Profile__c,'Acquire Career Champion');
    }
}