public with sharing class CourseDatesMap{

/************************
    * Trigger:      CourseDatesMap
    * Author:       Philip Clark
    * Created:      18/01/2015

    * Version:      1.0
    * Change Log:  
    *
    * 
    ************************/

  // Variables
  private static Boolean alreadyCheckedDates = false;
    
    /**
    * Description:  When an Opportunity is set to forms completed and the Enrolment Date or Start Date are populated, find 
    * a matching Course Date record for the Opportunity's related Course. Update appropriate fields on the Opportunity 
    * with this data.
    */
    
    public static boolean hasAlreadyCheckedDates() {
        return alreadyCheckedDates;
    }
    
    public static void setAlreadyCheckedDates() {
        alreadyCheckedDates = true;
    }

    public static integer daysToAvoidWeekend(Date courseDate) {
        Date monday = Date.newInstance(1900, 1, 1);
        Integer daysToAdd = 0;
        if (Math.mod(monday.daysBetween(courseDate), 7) == 5) 
        {
            system.debug('Date is a Saturday, adding 2 days');
            daysToAdd = 2;
        } else if (Math.mod(monday.daysBetween(courseDate), 7) == 6)
        {
            system.debug('Date is a Sunday, adding 1 day');
            daysToAdd = 1;
        }
        
        return daysToAdd;
    }
    
    public static integer roundUpToDayOfWeek(Date courseDate, String dayOfWeek) {
        Date monday = Date.newInstance(1900, 1, 1);
        Date tuesday = Date.newInstance(1900, 1, 2);
        Date wednesday = Date.newInstance(1900, 1, 3);
        Date thursday = Date.newInstance(1900, 1, 4);
        Date friday = Date.newInstance(1900, 1, 5);
        Date saturday = Date.newInstance(1900, 1, 6);
        Date sunday = Date.newInstance(1900, 1, 7);
        Integer daysToAdd = 0;
        if (dayOfWeek == 'Monday') {
            daysToAdd =  7 - (Math.mod(monday.daysBetween(courseDate), 7)); //2 = 5
        } else if (dayOfWeek == 'Tuesday') {
            daysToAdd =  7 - (Math.mod(tuesday.daysBetween(courseDate), 7)); //1 = 6
        } else if (dayOfWeek == 'Wednesday') {
            daysToAdd =  7 - (Math.mod(wednesday.daysBetween(courseDate), 7)); //0 = 7
        } else if (dayOfWeek == 'Thursday') {
            daysToAdd =  7 - (Math.mod(thursday.daysBetween(courseDate), 7)); //6 = 1
        } else if (dayOfWeek == 'Friday') {
            daysToAdd =  7 - (Math.mod(friday.daysBetween(courseDate), 7)); //5 = 2
        } else if (dayOfWeek == 'Saturday') {
            daysToAdd =  7 - (Math.mod(saturday.daysBetween(courseDate), 7)); //4 = 3
        } else if (dayOfWeek == 'Sunday') {
            daysToAdd =  7 - (Math.mod(sunday.daysBetween(courseDate), 7)); //3 = 4
        }
        if (daysToAdd == 7) {daysToAdd = 0;}
        return daysToAdd;
    }
    
    public static void mapCourseDates(List<Opportunity> newOppList){
        RecordType recType = [Select id, name, DeveloperName From RecordType WHERE SobjectType = 'Opportunity' AND DeveloperName = 'Default' LIMIT 1];
        system.debug('start mapCourseDates');
        
        //map Courses related to Opps
        List<Id> CourseIds = new List<Id>();
        List<Date> CourseStartDates = new List<Date>();
        for(Opportunity opp: newOppList)
        {
            CourseIds.add(opp.Course_LookUp__c);
            CourseStartDates.add(opp.Course_Start_Date__c);
        }        
        Map<Id, Course__c> courseMap = new Map<Id, Course__c>([Select Id, Name, No_of_Days_Enrolment_to_Start__c, No_of_Days_Start_to_Census_1__c, No_of_Days_Start_to_Census_2__c, Time_Start_to_Course_Complete__c, Unit_of_Time__c, Allow_Weekends__c, Census_1_Day_of_Week__c 
			From Course__c where Id in :CourseIds]);    
        
       
        
        for(Opportunity opp: newOppList){
            //Get Course related to this Opp
            Course__c cou = courseMap.get(opp.Course_LookUp__c);
            
            //check if course opportunity
            system.debug('check if course opportunity');
            if(opp.RecordTypeId == recType.Id && opp.Course_LookUp__c != NULL){
                system.debug('Correct opp recordtype and course present');
                system.debug('Opp course lookup: ' + opp.Course_LookUp__c);
                
                         
                // test if course fields for formula are populated
                if (cou.No_of_Days_Start_to_Census_1__c!=null){
                    system.debug('using CourseDatesFormula');
                    // Populate opp dates with formula based on Start Date
                    // Flag opportunity dates have been set using CourseDatesMap
                    opp.Date_course_dates_auto_applied__c = Datetime.now();
                    // populate Census 1 with formula
                    opp.X1st_census_date__c = opp.Course_Start_Date__c.addDays((Integer) (cou.No_of_Days_Start_to_Census_1__c));
                    system.debug('Original first census date: ' + opp.X1st_census_date__c);
                    opp.X1st_census_date__c = opp.X1st_census_date__c.addDays(roundUpToDayOfWeek(opp.X1st_census_date__c,cou.Census_1_Day_of_Week__c));
                    if (cou.Allow_Weekends__c == FALSE) {
                    	opp.X1st_census_date__c = opp.X1st_census_date__c.addDays(daysToAvoidWeekend(opp.X1st_census_date__c));
                    }
                    system.debug('Corrected first census date: ' + opp.X1st_census_date__c);
                    // populate Census 2 with formula if present
                    if (cou.No_of_Days_Start_to_Census_2__c != NULL){
                        opp.X2nd_census_date__c = opp.Course_Start_Date__c.addDays((Integer) (cou.No_of_Days_Start_to_Census_2__c));
                        if (cou.Allow_Weekends__c == FALSE) {
                        	opp.X2nd_census_date__c = opp.X2nd_census_date__c.addDays(daysToAvoidWeekend(opp.X2nd_census_date__c));
                        }
                    }
                    
                    // populate Course Completion Date with formula if present
                    if (cou.Time_Start_to_Course_Complete__c != NULL && cou.Unit_of_Time__c != NULL){
                        if (cou.Unit_of_Time__c == 'Days'){
                            opp.Course_Completion_Date__c = opp.Course_Start_Date__c.addDays((Integer) (cou.Time_Start_to_Course_Complete__c));
                        } else if (cou.Unit_of_Time__c == 'Months'){
                            opp.Course_Completion_Date__c = opp.Course_Start_Date__c.addMonths((Integer) (cou.Time_Start_to_Course_Complete__c));
                        }
					if (cou.Allow_Weekends__c == FALSE) {
                        opp.Course_Completion_Date__c = opp.Course_Completion_Date__c.addDays(daysToAvoidWeekend(opp.Course_Completion_Date__c));
                    }
                    }
                    system.debug('Completed courseDatesFormula with first census date ' + opp.X1st_census_date__c);
                    
                } else {
                    system.debug('using CourseDatesTable');
                    // Populate opp dates with table based on Start Date
                    //find matching course date
                    Map<id,Course_Date__c> mapCourseDate=new Map<id,Course_Date__c>
                        ([select Id, name, Start_Date__c, Census_Date_1__c, Census_Date_2__c, Complete_Date__c, Course__c from Course_Date__c 
                          where Course__c =:cou.Id AND Start_Date__c =: opp.Course_Start_Date__c]);
                    //error if course dates returned are less or greater than 1
                    if (mapCourseDate.size() == NULL) {
                        throw new applicationException('No matching course dates found.');
                    }
                    if (mapCourseDate.size() > 1) {
                        throw new applicationException('More than 1 matching course date found.');
                    }
                    for(Id courseDateId : mapCourseDate.keySet()) {
                        Course_Date__c courseDate = mapCourseDate.get(courseDateId);
                        //populate opp Census 1 with Course Date Census 1
                        // Flag opportunity dates have been set using CourseDatesMap
                        opp.Date_course_dates_auto_applied__c = Datetime.now();
                        opp.X1st_census_date__c = courseDate.Census_Date_1__c;
                        //populate opp Census 2 with Course Date Census 2 if present
                        if (courseDate.Census_Date_2__c != NULL){
                            opp.X2nd_census_date__c = courseDate.Census_Date_2__c;
                        }
                        // populate Opp Course Completion Date with Course Date Complete Date if present
                        if (courseDate.Complete_Date__c != NULL){
                            opp.Course_Completion_Date__c = courseDate.Complete_Date__c;
                        }
                    }
                    system.debug('Completed courseDatesTable');
                }
            } else {
                system.debug('failed test of opp recordtype and opp course: ' + opp.Course_LookUp__c);
            }
        }
    }
    public class applicationException extends Exception{}    

}