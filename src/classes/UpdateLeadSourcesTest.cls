@isTest
public class UpdateLeadSourcesTest{
static testMethod void LeadSourcesUpdateTest(){
//Creating data for insert
Lead L= new Lead(LastName='ABC', FirstName='Test', Gender__c='Female', Lead_Sources_for_Career_Advisor__c='Carlton FC',Phone='0234567890');
//inserting to call trigger UpdateLeadSources
insert L;
//Querying record created
L= [Select LeadSource from Lead where Id= :L.Id];
//Testing if the trigger ran successfully before update
System.assertEquals('Carlton FC', L.LeadSource);

//Creating test data for testing before update
Lead M= new Lead(LastName='ABC', FirstName='Test', Gender__c='Female',Phone='0234567890');
insert M;

// Updating to call trigger
M.Lead_Sources_for_Career_Advisor__c='Open Top';
update M;

// Testing
M=[Select LeadSource from Lead where Id= :M.Id];
//Testing if the trigger ran successfully before update
System.assertEquals('Open Top',M.LeadSource);
}
}