public class attachmentUtil {
	
	public static boolean disableTrigger {get{if(disableTrigger == null) disableTrigger = false; return disableTrigger;} set;}
	
	public static void prefixLeadAttachments(list<Attachment> newList) {
		//Get the attachment IDs
		set<Id> setIDs = new set<Id>();
		for(Attachment a : newList) setIDs.add(a.Id);
		//Get the fields needed from the attachments
		list<Attachment> listAtt = new list<Attachment>();
		listAtt = [select Id, Name, ParentId from Attachment where Id in: setIDs];
		list<Attachment> listUpdateAtt = new list<Attachment>();
		//Loop through the attachments and see if they are lead attachments
		for(Attachment a : listAtt) {
			if(a.ParentId.getSobjectType()==Lead.SobjectType && !a.Name.startsWith('Lead_')) { //We have a lead attachment without a prefix
				a.Name = 'Lead_' + a.Name;
				listUpdateAtt.add(a);
			}
		}
		//Update the lead attachments
		if(!listUpdateAtt.isEmpty()) {
			try {
				disableTrigger = true;
				update listUpdateAtt;
			} catch(Exception ex) {
				system.debug('Error renaming lead attachments:'+ex.getMessage());
			} finally {
				disableTrigger = false;
			}
		}
	}
}