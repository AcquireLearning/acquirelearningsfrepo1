public class NotesAndAttachmentTriggerHandler{

    //this handler method is used to count number of attachments in Lead object
    public static void CountAttachementsOnLead(List<Attachment> newAttchList, List<Attachment> oldAttchList, Boolean isInsert, Boolean isDelete){
    
        //list of Leads to be updated
        List<Lead> leadUpdateList = new List<Lead>();
        
        //when attchment is inserted
        if(isInsert){
            try{
                //list of attachment parent Ids
                List<Id> leadIds = new List<Id>();
                for(Attachment attachmnt: newAttchList){
                    leadIds.add(attachmnt.parentId);
                }
            
                //SOQL to get list of leads from attachment parent Ids.
                List<Lead> leadList = [Select Id, name, Has_Notes_and_Attachments__c, Attachment_Count__c From Lead Where id IN: leadIds];
            
                if(leadList!=null && leadList.size()>0){
                    for(Lead lead: leadList){
                        if(lead.Attachment_Count__c != null && lead.Attachment_Count__c > 0 ){
                            lead.Attachment_Count__c += 1;
                        }else{
                            lead.Attachment_Count__c = 1;
                        }
                        lead.Has_Notes_and_Attachments__c = true;
                        leadUpdateList.add(lead);
                    }
                    update leadUpdateList;
                }            
            }catch(Exception ex){
                System.debug('Exception occured while insert attchment count::-'+ex);
            }
        } 
        //when attchment is deleted
        else if(isDelete){
            try{
                //list of attachment parent Ids
                List<Id> leadIds = new List<Id>();
                for(Attachment attachmnt: oldAttchList){
                    leadIds.add(attachmnt.parentId);
                }
                system.debug(newAttchList);
                system.debug(oldAttchList);
                //SOQL to get list of leads from attachment parent Ids.
                List<Lead> leadList = [Select Id, name, Has_Notes_and_Attachments__c, Note_Count__c, Attachment_Count__c From Lead Where id IN: leadIds];
        
                if(leadList!=null && leadList.size()>0){
                    for(Lead lead: leadList){
                        lead.Attachment_Count__c -= 1;
                        system.debug('lead.Note_Count__c++++++' + lead.Note_Count__c);
                        system.debug('lead.Attachment_Count__c%%%%%%' + lead.Attachment_Count__c);
                        if((lead.Note_Count__c == 0  && lead.Attachment_Count__c == 0 )){
                            lead.Has_Notes_and_Attachments__c = false;
                        }
                        leadUpdateList.add(lead);
                    }
                    update leadUpdateList;                
                }
            }catch(Exception ex){
                System.debug('Exception occured while delete attchment count::-'+ex);
            }
        }
    }   
    
    public Static void CountNotesOnLead(List<Note> newNoteList, List<Note> oldNoteList, Boolean isInsert, Boolean isDelete){ 
        List<Lead> leadUpdateList2 = new List<Lead>();
        if(isInsert){
            try{
                //list of note parent Ids
                List<Id> leadIds2 = new List<Id>();
                for(Note Notes: newNoteList){
                    leadIds2.add(Notes.parentId);
                }
            
                //SOQL to get list of leads from note parent Ids.
                List<Lead> leadList = [Select Id, name, Has_Notes_and_Attachments__c, Note_Count__c, Attachment_Count__c From Lead Where id IN: leadIds2];
            
                if(leadList!=null && leadList.size()>0){
                    for(Lead lead: leadList){
                        if(lead.Note_Count__c != null && lead.Note_Count__c > 0 ){
                            lead.Note_Count__c += 1;
                        }else{
                            lead.Note_Count__c = 1;
                        }
                        lead.Has_Notes_and_Attachments__c = true;
                        leadUpdateList2.add(lead);
                    }
                    update leadUpdateList2;
                }            
            }catch(Exception ex){
                System.debug('Exception occured while insert note count::-'+ex);
            }
        } 
        //when note is deleted
        else if(isDelete){
            try{
                //list of notes parent Ids
                List<Id> leadIds2 = new List<Id>();
                for(Note Notes: oldNoteList){
                System.debug('Note Notes: oldNoteList#######'+oldNoteList);
                    leadIds2.add(Notes.parentId);
                }
            
                //SOQL to get list of leads from note parent Ids.
                List<Lead> leadList2 = [Select Id, name, Has_Notes_and_Attachments__c,Attachment_Count__c, Note_Count__c From Lead Where id IN: leadIds2];
                System.debug('leadList2^^^^^^^'+leadList2);
                if(leadList2!=null && leadList2.size()>0){
                    for(Lead lead: leadList2){
                        lead.Note_Count__c -= 1;
                        System.debug('Note_Count__c******'+lead.Note_Count__c);
                        System.debug('Attachment_Count__c&&&&&&&&&'+lead.Attachment_Count__c);
                        if((lead.Note_Count__c == 0  && (lead.Attachment_Count__c == null || lead.Attachment_Count__c == 0) )){
                        
                            lead.Has_Notes_and_Attachments__c = false;
                        }
                        leadUpdateList2.add(lead);
                    }
                    update leadUpdateList2;                
                }
            }catch(Exception ex){
                System.debug('Exception occured while delete note count::-'+ex);
            }
        }
    }
}