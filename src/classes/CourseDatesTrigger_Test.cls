@isTest
public class CourseDatesTrigger_Test {

    public static testmethod void CourseFormulaTest(){    

        RecordType provAccRecType = [Select id from RecordType where sObjectType = 'Account' AND DeveloperName = 'Education_Provider_Account' LIMIT 1];
        RecordType perAccRecType = [Select id from RecordType where sObjectType = 'Account' AND DeveloperName = 'PersonAccount' LIMIT 1];
        RecordType oppRecType = [Select id From RecordType where sObjectType = 'Opportunity' AND DeveloperName = 'Default' LIMIT 1];
        
        Test.startTest();
        
        //Create Education Provider
        Account provAcc = new Account();
        provAcc.recordtypeid=provAccRecType.id;
        provAcc.Name='Test Provider';
        insert provAcc ;
        
        //Create formula Course
        Course__c fCou = new Course__c();
        fCou.Name ='Formula Course';
        fCou.Account__c = provAcc.Id;
        fCou.No_of_Days_Start_to_Census_1__c = 5;
        fCou.No_of_Days_Start_to_Census_2__c = 10;
        fCou.Time_Start_to_Course_Complete__c = 15;
        fCou.Unit_of_Time__c = 'Days';
        insert fCou;
                     
        //Create student
        Account perAcc = new Account();
        perAcc.recordtypeid = perAccRecType.id;
        perAcc.LastName='Test';
        perAcc.Opportunity_Stage__c = 'Course in Progress';
        insert perAcc ;
        
        //Create formula course Opp
        Opportunity formOpp = new Opportunity();
        formOpp.recordtypeid = oppRecType.id;
        formOpp.Name = 'Formula Course Opp';
        formOpp.accountId = perAcc.id;
        formOpp.Education_Provider_Account__c = provAcc.id;
        formOpp.Course_LookUp__c = fCou.id;
        formOpp.StageName = 'Enrolled - Forms Completed';
        formOpp.Admission_Recommendation__c = 'test';
        formOpp.Study_Option__c = 'Online';
        formOpp.CloseDate = system.today();
        formOpp.Course_Start_Date__c = Date.newInstance(2015, 06, 01);
        insert formOpp ;
                
        Opportunity formOppSaved = [SELECT Id, Name, X1st_census_date__c, X2nd_census_date__c, Course_Completion_Date__c FROM Opportunity WHERE Id=: formOpp.id];
        system.debug('formOpp census dates: ' + formOppSaved.X1st_census_date__c + ', ' + formOppSaved.X2nd_census_date__c + ', ' + formOppSaved.Course_Completion_Date__c);
        System.assert(formOppSaved.X1st_census_date__c==Date.newInstance(2015, 06, 08),'Census 1 date is populated correctly by formula and not on a weekend');
        System.assert(formOppSaved.X2nd_census_date__c==Date.newInstance(2015, 06, 11),'Census 2 date is populated correctly by formula');
        System.assert(formOppSaved.Course_Completion_Date__c==Date.newInstance(2015, 06, 16),'Course completion date is populated correctly by formula');

        test.stopTest();
    }
    
    public static testmethod void CourseTableTest(){ 
    	
        RecordType provAccRecType = [Select id from RecordType where sObjectType = 'Account' AND DeveloperName = 'Education_Provider_Account' LIMIT 1];
        RecordType perAccRecType = [Select id from RecordType where sObjectType = 'Account' AND DeveloperName = 'PersonAccount' LIMIT 1];
        RecordType oppRecType = [Select id From RecordType where sObjectType = 'Opportunity' AND DeveloperName = 'Default' LIMIT 1];
        
        Test.startTest();
        
        //Create Education Provider
        Account provAcc = new Account();
        provAcc.recordtypeid=provAccRecType.id;
        provAcc.Name='Test Provider';
        insert provAcc ;
        
        //Create table Course
        Course__c tCou = new Course__c();
        tCou.Name ='Table Course';
        tCou.Account__c = provAcc.id;
        insert tCou;
        
        //Create table course Dates
        Course_Date__c tCouDate = new Course_Date__c();
        tCouDate.Start_Date__c = Date.newInstance(2015, 06, 01);
        tCouDate.Census_Date_1__c = Date.newInstance(2015, 06, 10);
        tCouDate.Census_Date_2__c = Date.newInstance(2015, 06, 20);
        tCouDate.Complete_Date__c = Date.newInstance(2015, 06, 30);
        tCouDate.Course__c = tCou.id;
    	insert tCouDate;
        
        //Create student
        Account perAcc = new Account();
        perAcc.recordtypeid = perAccRecType.id;
        perAcc.LastName='Test';
        perAcc.Opportunity_Stage__c = 'Course in Progress';
        insert perAcc ;
        
        //Create table course Opp
        Opportunity tableOpp = new Opportunity();
        tableOpp.recordtypeid = oppRecType.id;
        tableOpp.Name = 'Table Course Opp';
        tableOpp.accountId = perAcc.id;
        tableOpp.Education_Provider_Account__c = provAcc.id;
        tableOpp.Course_LookUp__c = tCou.id;
        tableOpp.StageName = 'Enrolled - Forms Completed';
        tableOpp.Admission_Recommendation__c = 'test';
        tableOpp.Study_Option__c = 'Online';
        tableOpp.CloseDate = system.today();
        tableOpp.Course_Start_Date__c = Date.newInstance(2015, 06, 01);
        insert tableOpp ;
        
        Opportunity tableOppSaved = [SELECT Id, Name, X1st_census_date__c, X2nd_census_date__c, Course_Completion_Date__c FROM Opportunity WHERE Id=: tableOpp.id];
        system.debug('tableOpp census dates: ' + tableOppSaved.X1st_census_date__c + ', ' + tableOppSaved.X2nd_census_date__c + ', ' + tableOppSaved.Course_Completion_Date__c);
        System.assert(tableOppSaved.X1st_census_date__c==Date.newInstance(2015, 06, 10),'Census 1 date is populated correctly by formula');
        System.assert(tableOppSaved.X2nd_census_date__c==Date.newInstance(2015, 06, 20),'Census 2 date is populated correctly by formula');
        System.assert(tableOppSaved.Course_Completion_Date__c==Date.newInstance(2015, 06, 30),'Course completion date is populated correctly by formula');

        test.stopTest();
    }
}